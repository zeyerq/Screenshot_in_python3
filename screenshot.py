#!/usr/bin/env python3.11

import os;
import sys;
from datetime import datetime;
try:
    import pyautogui;
except:
    print("Please install missing packages: apt install scrot python3-tk python3-dev -y");
    sys.exit("Please install missing library: python3.11 -m pip install --break-system-packages PyAutoGUI");

myScreenshot = pyautogui.screenshot();

dh = datetime.now();
date_and_time = dh.strftime("%m_%d_%Y_%H_%M_%S");

def start():
    myScreenshot = pyautogui.screenshot();
    myScreenshot.save(f"screenshot_{date_and_time}.jpg", "jpeg");

if __name__ == "__main__":
    start();
